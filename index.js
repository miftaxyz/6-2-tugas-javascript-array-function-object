
//Soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort().forEach(function(item){
   console.log(item)
})

//Soal 2

function introduce(array){
	return `Nama saya  ${array.name }, umur saya  ${array.age} tahun, alamat saya di ${array.address} , dan saya punya hobby yaitu ${array.hobby}!`
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

//Soal 3

function hitung_huruf_vokal(string){
	var numberVokal = 0;
	var vokal = ['a','i','u','e','o'];
	var temp = string.toLowerCase().split("");
	for (i=0; i<string.length;i++){
		for(j=0; j<vokal.length;j++){
			if(temp[i]==vokal[j]){
				numberVokal+=1;
			}
		}
	}
	return numberVokal
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

//Soal 4

function hitung(number){
	var temp=[];
	for (i=-2;i<=10;i+=2){
		temp.push(i);
	}
	return temp[number]
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8

